
<!-- <iframe src="//player.bilibili.com/player.html?aid=347852412&bvid=BV1rR4y1f7mA&cid=896199338&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true" style="width: 640px; height: 430px; max-width: 100%"> </iframe> -->
![](http://user-assets.sxlcdn.com/images/805944/FsdVhyfxszA5LgymzUs4n-Cdqj7s.jpeg?imageMogr2/strip/auto-orient/thumbnail/1920x9000%3E/quality/90!/interlace/1/format/jpeg)

![](http://user-assets.sxlcdn.com/images/805944/FsGMQC5y3tJRIl2dwHaJ0RQ9uYyt.jpeg?imageMogr2/strip/auto-orient/thumbnail/1920x9000%3E/quality/90!/interlace/1/format/jpeg)

## 履历
**王冰冰**

中国内地记者、媒体人。  
 
王冰冰，出生于吉林省长春市，中国内地女记者、主持人 ，毕业于吉林大学播音专业。  
 
别称：**查干湖恶霸**、胖头鱼公主等。  
 
2012年，进入吉林日报社《城市晚报》实习  

2016年，进入中央广播电视总台工作  

2019年，担任“2019年中央广播电视总台春节联欢晚会”吉林分会场采访记者。  

2020年11月2日，主持的“青年大学习”网上主题团课上线；  

11月18日，参加央视新闻和BOSS直聘联合推出的《“职”为你来》秋招公益直播   

2021年2月28日，获得“2020微博之夜年度热点人物榜”21名 ；  

6月2日起，为央视网出品军旅纪录片《新兵请入列》担任配音。  

8月5日，参加央视网出品的新青年生活分享类综艺节目《你好生活第三季》播出 ；  

8月13日，参加由央视频推出的主播新媒体才艺秀《央young之夏》播出 ；  

9月10日，参加央视新闻推出的《CBD汽车直播夜》直播；  

9月中下旬，参加中华人民共和国第十四届运动会的多项赛事的报道；  

10月起，在央视新闻《全国红色故事讲解大赛》中作为主持人：  

10月16日，参加上海举行的“中国时装盛典”并参与“五美”展示；  

10月中旬，参与CGTN“海上看中国”系列报道；  

11月5日-10日，对第四届上海进博会进行全方面的报道；  

12月12日，参与共青团主办的《青春出发2021》第一季第一集，去往吉林省延边朝鲜族自治州;  

12月，参加由央视频推出的节目《冬日暖央young》，以推动冬奥宣传；  

第五届“中国青年好网民”优秀故事评选中当选；  

2022年，1月1日，参加总台首档青年新年分享类节目《@青春，2022！》  

1月，为迎接北京冬奥会，《冰冰带你上冰雪》《中国冰雪盛典》等衍生节目推出，来通过王冰冰亲身体验冰雪运动来普及冬奥和冰雪知识；  

小年夜参加2022年中央电视台《网络春晚》；  

哔哩哔哩2021年最佳新人奖获得者； 

2021中国品牌人物500强;  

1月，在央视频和哔哩哔哩上线了自己的运动员零距离式采访节目《闪闪发光的少年》；  

1月31日（除夕），对2022中央广播电视总台春节联欢晚会进行探班，并在CCTV13除夕特别节目《龙腾虎跃中国年》中开始专题“冰冰带你探春晚”；  

2月7日，“央young”系列《开工喜央young》播出；

7月13日，总台农业农村中心和成都电视台制作并推广的慢综艺《冰冰带你来种田》正式启动，总台记者王冰冰将与嘉宾们一起享受农耕慢生活；  

11月21日，央视频3周年和世界杯开幕式央视频预热节目《央视频之夜》直播，王冰冰担任主持人之一；  

11月22日，参与文化知识节目《国之大雅》播出。 

11月下旬至12月中旬，央视频推出《欧雷欧雷陪看团》和《球迷梦之夜》两档卡塔尔世界杯相关直播节目，王冰冰担任嘉宾和主持人；  

12月16日，《闪闪发光的少年》第二季正式在央视频和哔哩哔哩上线！

2023年  
1月11日至20日，由王冰冰配音的纪录片《奇妙中国》将在CCTV9、央视频等平台播出；  

1月14日小年夜，参加中央广播电视总台2023年《网络春晚》；  

1月15日，央视新闻联合科技部人才与科学普及司、中国科协科普部和总台北京总站举办的《酷啊未来 中国科技创新之夜》，王冰冰参与；  

1月19日，王冰冰作为中央广播电视总台2023央视频重点节目片单发布会主持人，同时发布众多央视频节目；  

癸卯新年前夕，王冰冰参与了中央广播电视总台春节联欢晚会的新媒体衍生节目《春晚进行时》和《young在春晚》；  

春节期间，王冰冰将参与CCTV3《吉聚欢喜》，用七部生活轻喜剧，串联出万千家庭蓬勃锐气的年味故事，烘托出当代中国醇厚馨香的欢乐节庆；  

第二届中国春兰节将于2月16日至2月19日举行，中央民族乐团团长赵聪,知名作家麦家 与总台记者王冰冰化身“春兰使者”，与你相约绍兴柯桥，共赴一场兰花之约；  

3月1日，总台青春分享类节目《青春@2023》在央视新闻、央视网播出，总台记者化身“青春电台”主播。


**（以上内容由[冰学网](https://mywbb.mysxl.cn/)综合各类信息编辑）**
***
## **美图奉上😘**

![](http://user-assets.sxlcdn.com/images/805944/FuPmbkF-MeeIgcF7YQJLay7fhp5r.jpeg?imageMogr2/strip/auto-orient/thumbnail/1920x9000%3E/quality/90!/interlace/1/format/jpeg)
![](http://user-assets.sxlcdn.com/images/805944/Fm0oCwyRA2JHaw-Gu8VmwWjTJ56Q.png?imageMogr2/strip/auto-orient/thumbnail/1920x9000%3E/quality/90!/format/png)
<!-- ![](http://user-assets.sxlcdn.com/images/805944/Fsg-4QEtJ-OvpdsH9BAqi1InddMg.png?imageMogr2/strip/auto-orient/thumbnail/1920x9000%3E/quality/90!/format/png) -->
![](http://user-assets.sxlcdn.com/images/805944/FiH-mvvoM8HvG7eaf7MmFS_um_NJ.jpeg?imageMogr2/strip/auto-orient/thumbnail/1920x9000%3E/quality/90!/interlace/1/format/jpeg)
![](http://user-assets.sxlcdn.com/images/805944/FgnNZ5bcMgm4WGqFIJhCZg4H_0Vy.jpeg?imageMogr2/strip/auto-orient/thumbnail/1920x9000%3E/quality/90!/interlace/1/format/jpeg)
![](http://user-assets.sxlcdn.com/images/805944/FjE5gIwWJknieTfYvPMWSRs6y84m.jpeg?imageMogr2/strip/auto-orient/thumbnail/!300x300r/gravity/Center/crop/300x300/quality/90!/interlace/1/format/jpeg)
![](http://user-assets.sxlcdn.com/images/805944/Fl2oW3qEPRL9bZPY1Z64JF__q8jE.png?imageMogr2/strip/auto-orient/thumbnail/!300x300r/gravity/Center/crop/300x300/quality/90!/format/png)
![](http://user-assets.sxlcdn.com/images/805944/FvQxnEkIj3NIlZ83YfDAV93rLxsh.jpeg?imageMogr2/strip/auto-orient/thumbnail/!300x300r/gravity/Center/crop/300x300/quality/90!/interlace/1/format/jpeg)
![](http://user-assets.sxlcdn.com/images/805944/FvNV5aPOLrFH1aEAoCN7cy-jvmme.jpeg?imageMogr2/strip/auto-orient/thumbnail/!200x200r/gravity/Center/crop/200x200/quality/90!/interlace/1/format/jpeg)

![](http://user-assets.sxlcdn.com/images/805944/FojYbVWHLoJNWAkrX23A45C9D2E6.jpeg?imageMogr2/strip/auto-orient/thumbnail/!300x300r/gravity/Center/crop/300x300/quality/90!/interlace/1/format/jpeg)
![](http://user-assets.sxlcdn.com/images/805944/FhNP2Flp78YRjFO1bMq0nwAem5px.jpeg?imageMogr2/strip/auto-orient/thumbnail/!300x300r/gravity/Center/crop/300x300/quality/90!/interlace/1/format/jpeg)

![](http://user-assets.sxlcdn.com/images/805944/FuA2YhHBIyRkCguA3hjDmq_AiHMj.jpeg?imageMogr2/strip/auto-orient/thumbnail/!200x200r/gravity/Center/crop/200x200/quality/90!/interlace/1/format/jpeg)
![](http://user-assets.sxlcdn.com/images/805944/Fr0b-lZkzH_QiqdQXqjcWH4qufmH.png?imageMogr2/strip/auto-orient/thumbnail/!200x200r/gravity/Center/crop/200x200/quality/90!/format/png)
![](http://user-assets.sxlcdn.com/images/805944/FtIBw15hQZOF3Xwh8s4XlXY5Npvi.jpeg?imageMogr2/strip/auto-orient/thumbnail/!200x200r/gravity/Center/crop/200x200/interlace/1/format/jpeg)
![](http://user-assets.sxlcdn.com/images/805944/FsdVhyfxszA5LgymzUs4n-Cdqj7s.jpeg?imageMogr2/strip/auto-orient/thumbnail/1920x9000%3E/quality/90!/interlace/1/format/jpeg)  
![](http://user-assets.sxlcdn.com/images/805944/FrqBmLVChZp-Ttjh99l1Z7rkTyIi.jpeg?imageMogr2/strip/auto-orient/thumbnail/1920x9000%3E/quality/90!/interlace/1/format/jpeg)
![Alt text](https://user-assets.sxlcdn.com/images/805944/FvKN_N6KJ68H25NIy92Hh09fk9QL.jpeg?imageMogr2%2Fstrip%2Fauto-orient%2Fthumbnail%2F1200x9000%3E%2Fquality%2F90%21%2Finterlace%2F1%2Fformat%2Fjpg)
![Alt text](http://user-assets.sxlcdn.com/images/805944/FoHMK09eBbwW-lNQsLonpNUmtOLJ.jpeg?imageMogr2%2Fstrip%2Fauto-orient%2Fthumbnail%2F1920x9000%3E%2Fquality%2F90%21%2Finterlace%2F1%2Fformat%2Fjpeg)
![Alt text](http://user-assets.sxlcdn.com/images/805944/Fr-1dCF-kODT5FdzFFshJXk57PpI.png?imageMogr2%2Fstrip%2Fauto-orient%2Fthumbnail%2F1920x9000%3E%2Fquality%2F90%21%2Fformat%2Fpng)
![](https://user-assets.sxlcdn.com/images/805944/FmPtQboeRUUUKB0w4V5i181pXyqc.jpeg?imageMogr2/strip/auto-orient/thumbnail/720x1440%3E/quality/90!/interlace/1/format/jpeg)
![Alt text](http://user-assets.sxlcdn.com/images/805944/FoT9bdBjAhu1GTOqoFK2U4Qa58iO.jpeg?imageMogr2%2Fstrip%2Fauto-orient%2Fthumbnail%2F1920x9000%3E%2Fquality%2F90%21%2Finterlace%2F1%2Fformat%2Fjpeg)