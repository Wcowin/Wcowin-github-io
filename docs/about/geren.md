---
hide:
#   - navigation
#   - toc
  - feedback
---

# 知足且上进，温柔且坚定
!!! note "About me"
    * Bilibili电子信息工程专业本科在读  :star2:
    * CMC一等奖获得者
    * 热爱(xiā)折腾技术 :computer: 
    * 喜欢村上春树📖，擅长羽毛球 :badminton:
    * [找到我](https://github.com/Wcowin) &#x1F4A1;→:simple-github:[GitHub](https://github.com/Wcowin){target="_blank"}  

<!-- ![](https://cn.mcecy.com/image/20221229/67d6c67f951fe04606acc08a1e77a81e.png) -->
<!-- [![Anurag's GitHub stats](https://github-readme-stats.vercel.app/api?username=Wcowin)](https://github.com/anuraghazra/github-readme-stats)   -->
![img](https://cn.mcecy.com/image/20230220/f10604560a2119667fb3aca1da299e1a.jpeg)

<p align="center">
  
  <a href="https://muselink.cc/Wcowin" target="_blank">
    <img src="https://cn.mcecy.com/image/20221229/67d6c67f951fe04606acc08a1e77a81e.png" alt="个人名片">
  </a>
</p>



[Send Email :fontawesome-solid-paper-plane:](mailto:<1135801806@qq.com>){ .md-button }

<ul>
  <li>
    当前页面浏览量:
    <span class="waline-pageview-count" />
  </li>
  <li>
    主页浏览量:
    <span class="waline-pageview-count" data-path="/docs/index.md" />
  </li>
</ul>
<script type="module">
  import { pageviewCount } from 'https://unpkg.com/@waline/client/dist/pageview.mjs';

  pageviewCount({
    serverURL: 'https://mk-docs-comments.vercel.app/',
    path: window.location.pathname,

    // 可选的，用于自定选择器，默认为 `'.waline-pageview-count'`
    // selector: 'waline-pageview-count',

    // 可选的，是否在获取时增加访问量，默认为 `true`
    // update: true,
  });
</script>
