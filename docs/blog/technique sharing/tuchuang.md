---
title: 
tags:
  - 技术分享
hide:
---



## 一、Github+cloudflare图床
比如我的图床：`https://myimgs.pages.dev/IMG/+我的图片名称`  
举个例子：https://myimgs.pages.dev/IMG/%E5%BE%AE%E4%BF%A1%202.png  

步骤1 :  
新建一个名为MyIMGS的github仓库（名称可随意）
[![https://www.hualigs.cn/image/63f357ba8a086.jpg](https://www.hualigs.cn/image/63f357ba8a086.jpg)](https://www.hualigs.cn/image/63f357ba8a086.jpg)
步骤2 ：  
打开[cloudflare](https://dash.cloudflare.com/)注册登陆  
[![https://www.hualigs.cn/image/63f35931b7561.jpg](https://www.hualigs.cn/image/63f35931b7561.jpg)](https://www.hualigs.cn/image/63f35931b7561.jpg)
打开左侧Pages,然后创建项目、连接到Git（刚刚创建的仓库），即可见到上图蓝色箭头指向的一个网址，我这个是myimgs.pages.dev

步骤3 ：
上传图片到Git仓库，我是在Git仓库建立了一个名为IMG的文件夹，在这个文件夹里放图片，比如我在IMG文件夹放了一张名为xigua,jpeg格式的图片，则这个照片的路径为:https://myimgs.pages.dev/IMG/xigua.jpeg  

[![https://www.hualigs.cn/image/63f35b7578e99.jpg](https://www.hualigs.cn/image/63f35b7578e99.jpg)](https://myimgs.pages.dev/IMG/xigua.jpeg)
## 二、免费图床
- [载涂图床](https://mcecy.com/)
- [遇见图床](https://www.hualigs.cn/)  
 
无脑上传图片，粘贴链接即可
[![https://www.hualigs.cn/image/63f35cf034f4e.jpg](https://www.hualigs.cn/image/63f35cf034f4e.jpg)](https://www.hualigs.cn/image/63f35cf034f4e.jpg)