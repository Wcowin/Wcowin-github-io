---
tags:
  - 技术分享
---
!!! Note "流程"
    * 准备工作：科学上网客户端如Clsah，圈X，小火箭(Shadowrocket)
    * 拥有一个订阅节点 
    * 将节点导入到科学上网客户端
    * 打开科学上网客户端开始网上冲浪～ 


[![https://www.hualigs.cn/image/641496182fe92.jpg](https://www.hualigs.cn/image/641496182fe92.jpg)](https://www.hualigs.cn/image/641496182fe92.jpg)

**客户端下载：  
ios: 需要使用外区Apple ID购买  
Shadowrocket:<https://www.rocketgirls.space> (推荐购买地址：https://www.rocketgirls.space/product/)
Android:<https://github.com/Kr328/ClashForAndroid>
PC/Mac:<https://github.com/Dreamacro/clash>**  

Shadowrocket/clash新手使用教程:<https://telegra.ph/departure-airport-05-29>  
Mac第三方客户端Clash教程:<https://flm88.blog/p/docs/mac-clash>  
Windows第三方客户端Clash教程:<https://flm88.blog/p/docs/windows-clash>

最后可以打开[这个页面](https://www.google.com/?hl=zh-CN&sa=X&ved=0ahUKEwjTmpfQ-u31AhVaEXAKHUcyBmcQnZcCCAc)即为成功：[谷歌](https://www.google.com/?hl=zh-CN&sa=X&ved=0ahUKEwjTmpfQ-u31AhVaEXAKHUcyBmcQnZcCCAc) 

推荐机场:material-vpn:
  
- [一元机场](https://一元机场.com/#/dashboard)  
- [三分机场](https://三分机场.xyz/)  
- [魔戒](https://mojie.me/#/dashboard)

> 声明：科学上网是为了更好的学习，因为很多国外网站要科学上网，如[Github](https://github.com/)等，**切勿使用做违法的事情，切勿相信墙外任何不当言论，反对台独，坚决支持维护国家领土主权**