---
hide:
  #- navigation # 显示右
  #- toc #显示左
  - footer
  - feedback
comments: false
gitalk: false
---


<!-- <script src="https://fastly.jsdelivr.net/gh/stevenjoezhang/live2d-widget@latest/autoload.js"></script> -->
# <font color= #4b78d8 >**Hello~**</font>
 <font face="宋体" color= #4b78d8 size=7 >欢迎访问我的<a href="https://wcowin.work/" target="_blank">个人网站</a>  </font>
  <!-- :fontawesome-brands-twitter:{ .twitter } -->

!!! Note "Tip"
    - 只分享好玩有趣的东西~
    - 通过主题和目录以打开文章  
        - PC端 在上方标签栏选择主题 在左侧目录选择文章
        - 移动端 点击左上角图标选择主题和文章   

    - 搜索关键词以打开文章
        

推荐文章:material-book::  

  - [如何如何注册ChatGPT](develop/ChatGPT.md)
  - [利用mkdocs部署静态网页至GitHub pages](blog/Mkdocs/mkdocs1.md)
  - [Homebrew国内如何自动安装(国内地址)(Mac & Linux)](blog/Mac/homebrew.md)
  - [好用/好玩网站分享](blog/Webplay.md)
  - [Mac/windows软件网站汇总](blog/macsoft.md )
  - [实用脚本分享](blog/technique sharing/jiaoben.md)
  


